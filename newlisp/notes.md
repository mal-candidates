- Arithmetic is worse than with JS because there's functions for
  integer math (truncates arguments) and float math (less precision)
- On top of that there's separate bigints, too
- Relative loading is emulated by using the `-w` option of the
  interpreter which changes the working directory for all load-like
  operations
- Closures need to be emulated by splicing in the argument or using
  contexts (which may be used to pass by reference)
- You may not redefine built-ins except by using `constant`, this
  seems to apply to bindings (such as function arguments) as well...
- There is an argv-like thing, but it returns the full invocation as
  opposed to omitting the already processed options (therefore I slice
  the already processed options away)
- Speaking of which, numbers in the funcall position are interpreted
  as sequence slicing, but sequences in the funcall position accept
  number arguments, too
- User-defined errors may only use strings, however you can eval an
  expression for figuring out that string, so implementing an
  errno-like construct is the way to go
- It's hard to spot typos because unbound symbols evaluate to `nil`
- Tokenization the right way is so bothersome that I decided to just
  find all regex matches instead and return the first group
