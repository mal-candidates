#include once "util.bas"

On Error Goto errorhandler

Dim start As Double = Util.Now()
Dim dict As Util.Dict
Dim r1 As Util.Rect
Dim i As Integer
dict.PushKV("a", "1")
dict.PushKV("b", "2")
r1.size = 6

Print "Words: ";
Util.ReadWords("> ").PrintList()
Print Using "The answer: &"; Util.TheAnswer()
' no closures
Print Using "Hostname: &"; Util.Hostname()
Print "ARGV: ";
Util.Argv().PrintList()
' no exception handling
' no apply
Print "Tokens: ";
Util.Tokenize("1 * (2 * 3) + 4").PrintList()
Print "Keys: ";
Util.Keys(dict).PrintList()
Print Using "Rect r1 size: &"; r1.size
Print "Doubling rect size..."
r1.size *= 2
Print Using "Rect r1 size: &"; r1.size
Print Using "Elapsed time: #.##ms"; Util.Now() - start
End 0

errorhandler:
Print Using "Error in &:&"; *Ermn(); Erl
End 1
