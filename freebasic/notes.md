- Feels like VB6 again, but way more powerful.
- Case-insensitive languages suck.
- Context-sensitive interpretation means that `=` doesn't necessarily
  do assignment, it may as well do comparison.
- For printing you're best off with formatted output and using
  semicolons.  If you use `print` with commas, you get wildly different
  formatting fo each item.
- To print without the trailing newline, use a trailing semicolon.
- Format strings are funny, you can do fixed digits, but that applies
  for the part before the decimal point as well.  On overflow a
  percentage sign is printed before the offending item.  There's
  "smart" formatting which doesn't seem better than just printing the
  number as is.
- Error handling sucks.  You can jump to a global/local error handler,
  but not even the global error number is preserved across the file
  boundary...
- File handling sucks.  Why do I have to care about retrieving a free
  FD?  Why do I have to use special syntax for it?  Why can it not be
  passed around?  This is ridiculous...
- Headers are mandatory for modularization, most surprisingly type
  declarations go there, not into the library.  I eventually gave up
  on splitting the util file into header and implementation, it's just
  a header now.
- User input is read in from the keyboard, not stdin.  This ruins
  redirection and features like EOF.  Furthermore this ruins the
  ability to use readline.  You can still read it from stdin by
  opening `/dev/stdin` though.
- Binding to foreign libraries is embarassingly simple.
