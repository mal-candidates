Type Greeter
    Private:
        target As String
    Public:
        Declare Constructor(target As String)
        Declare Sub Apply
End Type

Constructor Greeter(target As String)
    This.target = target
End Constructor

Sub Greeter.Apply
    Print "Hello " & target & "!"
End Sub

Function MakeGreeter(target As String) As Greeter
    Return Greeter(target)
End Function

MakeGreeter("World").Apply
