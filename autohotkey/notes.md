- Scoping is fucked, a global variable isn't visible in local
  functions unless declared inside the function (or enabling some
  screwed up mode making everything global)
- Backticks are used as escape character
- There's a stupid split in the language between expressions (using
  functions) and legacy-style commands, AHK2 is a
  backward-incompatible version favoring the former
- No errors/warnings by default
- Sort of weakly typed (number strings are coerced to numbers and they
  use "caching" to deal with this in a somewhat performant way)
- No line terminators, semicolon used as comment character
- Trim functions default to space and tab, not all whitespace
- Division either gives you always floats or always integers
- Case-insensitive identifiers
- For-loops exist, but they only allow iterating with key or key and
  value, not just value
- Booleans are actually 0 and 1, but there's true/false variables
  evaluating to them
- Arrays are BASIC-like (don't have to start at 1, but commonly do)
- The equality operator is `=` (hello BASIC) because `:=` performs
  assignment, however `==` exists for case-sensitive string comparison
- Closures are sort of supported with a JS-style `bind` function that
  performs partial application. AHK2 fixes this by supporting
  Python-style closures
- There's only one kind of exception, but AHK2 allows catching specific
  error types
