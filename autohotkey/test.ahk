#Warn All, StdOut
#Include <util>

then := Now()

dict := {"a": 1, "b": 2}
r1 := new Rect(6)

Sum(args*) {
    acc := 0

    for _, arg in args {
        acc += arg
    }

    return acc
}

stdout.WriteLine("Reading words...")
stdout.WriteLine("Words: " . Join(", ", ReadWords("> ")))
stdout.WriteLine("The answer: " . TheAnswer())
stdout.WriteLine(MakeGreeter("World").Call())
stdout.WriteLine("Hostname: " . Hostname())
stdout.WriteLine("ARGV: " . Join(", ", Argv()))
stdout.WriteLine("Error: " . FailGracefully())
stdout.WriteLine("Sum: " . Splat(Func("Sum"), [1, 2, 3, 4]))
stdout.WriteLine("Tokens: " . Join(", ", Tokenize("1 * (2 * 3) + 4")))
stdout.WriteLine("Keys: " . Join(", ", Keys(dict)))
stdout.WriteLine("Rect r1 size: " . r1.size)
stdout.WriteLine("Doubling rect size...")
r1.size *= 2
stdout.WriteLine("Rect r1 size: " . r1.size)
stdout.WriteLine(Format("Elapsed time: {:.2f}ms", Now() - then))
