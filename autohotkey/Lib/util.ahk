stdin := FileOpen("*", "r")
stdout := FileOpen("*", "w")

clockFreq := 0
clockStart := 0
DllCall("QueryPerformanceFrequency", "Int64*", clockFreq)
DllCall("QueryPerformanceCounter", "Int64*", clockStart)

Join(sep, items) {
    str := ""

    for key, item in items {
        if (key == 1) {
            str .= item
        } else {
            str .= sep . item
        }
    }

    return str
}

ReadWords(prompt) {
    global stdin
    words := []

    while (word := stdin.ReadLine()) {
        words.push(RTrim(word, "`r`n"))
    }

    return words
}

TheAnswer() {
    return (6 // 2 ) * (7 + 8) - 3
}

Greeter(name) {
    return "Hello " . name
}

MakeGreeter(name) {
    return Func("Greeter").Bind(name)
}

Hostname() {
    content := ""
    FileRead, content, % "/etc/hostname"
    return RTrim(content, "`r`n")
}

Argv() {
    return A_Args
}

FailGracefully() {
    try {
        throw Exception("Test")
    } catch e {
        return e.Message
    }
}

Splat(f, args) {
    return f.Call(args*)
}

Tokenize(input) {
    tokens := []
    match := ""
    i := 1

    while (RegExMatch(input, "O)\s*([-()+*/]|\d)", match, i) != 0) {
        if (i != match.Pos(0)) {
            return [input]
        }

        tokens.push(match.Value(1))
        i += match.Len(0)
    }

    return tokens
}

Keys(dict) {
    result := []

    for key in dict {
        result.push(key)
    }

    return result
}

Now() {
    global clockFreq, clockStart
    clockNow := 0
    DllCall("QueryPerformanceCounter", "Int64*", clockNow)
    return (clockNow - clockStart) / clockFreq * 1000
}

class Rect {
    __New(size) {
        this.size := size
    }
}
