#include "crt/ctype.bi"
#include "crt/stdio.bi"
#include "crt/string.bi"
Namespace Util

Public Type StringList
    Dim As String storage(Any)
    Declare Constructor
    Declare Function Length() As Integer
    Declare Function At(i As Integer) As String
    Declare Function Pop() As String
    Declare Sub Push(s As String)
    Declare Sub PrintList()
End Type

Constructor StringList
    Redim storage(0) As String
End Constructor

Function StringList.Length() As Integer
    Return Ubound(This.storage)
End Function

Function StringList.At(i As Integer) As String
    Return This.storage(i)
End Function

Sub StringList.Push(s As String)
    Dim size As Integer
    size = Ubound(This.storage) + 1
    Redim Preserve This.storage(Lbound(This.storage) To size)
    This.storage(size - 1) = s
End Sub

Function StringList.Pop() As String
    Dim s As String
    Dim size As Integer
    size = Ubound(This.storage)
    If size > 0 Then
        s = This.Storage(size - 1)
        Redim Preserve This.storage(Lbound(This.storage) to size - 1)
    End If
    Return s
End Function

Sub StringList.PrintList()
    Dim i As Integer
    For i = 0 To Ubound(This.storage) - 1
        If i <> 0 Then Print ", ";
        Print This.storage(i);
    Next
    Print
End Sub

Public Type Dict Extends StringList
    Declare Sub PushKV(key As String, value As String)
End Type

Sub Dict.PushKV(key As String, value As String)
    This.Push(key)
    This.Push(value)
End Sub

Public Type Rect
    size As Integer
End Type

Dim As FILE Ptr stdin = null
Function ReadWord(prompt As String) As String
    If stdin = null Then stdin = fopen("/dev/stdin", "r")
    Return *fgets(prompt, 1024, stdin)
End Function

Public Function ReadWords(prompt As String) As StringList
    Dim words As StringList
    Do
        Dim word As String = ReadWord(prompt)
        If Len(word) = 0 Then Return words
        words.Push(Rtrim(word, !"\n"))
    Loop
End Function

Public Function TheAnswer() As Integer
    Return 3 * 13 + 3
End Function

' no closures

Public Function Hostname() As String
    Dim f As Integer
    Dim host As String
    f = FreeFile()
    Open "/etc/hostname" For Input As #f
    If Err > 0 Then Error Err
    Line Input #f, host
    Close #f
    Return host
End Function

Public Function Argv() As StringList
    Dim args As StringList
    Dim i As Integer = 1
    Do
        Dim arg As String = Command(i)
        If Len(arg) = 0 Then Return args
        args.Push(arg)
        i += 1
    Loop
End Function

' no exception handling
' no apply

Public Function Tokenize(s As String) As StringList
    Dim tokens As StringList
    Dim token As String
    Dim toksz As Integer
    While (Len(s) > 0)
        toksz = strspn(s, " ")
        If (toksz > 0) Then
            s = Mid(s, 1 + toksz)
        End If
        If (isdigit(Asc(s, 1))) Then
            toksz = strspn(s, "1234567890")
            token = Left(s, toksz)
            tokens.Push(token)
            s = Mid(s, 1 + toksz)
        Elseif (strspn(s, "+-*/()")) Then
            token = Left(s, 1)
            tokens.Push(token)
            s = Mid(s, 2)
        Else
            toksz = strcspn(s, " 1234567890+-*/()")
            token = Left(s, toksz)
            tokens.Push(token)
            s = Mid(s, 1 + toksz)
        End If
    Wend
    Return tokens
End Function

Public Function Keys(d As Dict) As StringList
    Dim ks As StringList
    Dim i As Integer
    For i = 0 To d.Length() - 1
        ks.Push(d.At(i))
        i += 1
    Next
    Return ks
End Function

Public Function Now() As Double
    Return Timer() * 1000
End Function

End Namespace
